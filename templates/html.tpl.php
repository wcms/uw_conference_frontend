<?php

/**
 * @file
 * Contains HTML template for custom frontend theme.
 */
$ga = "<script>";
if (variable_get('uw_cfg_google_analytics_account') || variable_get('google_analytics_enable') == 1) {
  $ga .= "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){\n" .
    " (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n" .
    "  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n" .
    " })(window,document,'script','//www.google-analytics.com/analytics.js','ga');\n";
}
if (!(variable_get('uw_cfg_google_analytics_account')) && variable_get('google_analytics_enable') == 1) {
  // Removed in favour of Google Tag Manager.
  // @code
  // $ga .= "ga('create', 'UA-51776731-1', 'auto');\n";
  // $ga .= "ga('send', 'pageview');\n";
  // @endcode
}
if (variable_get('uw_cfg_google_analytics_account') && variable_get('google_analytics_enable') == 0) {
 $ga .= "ga('create', '" . check_plain(variable_get('uw_cfg_google_analytics_account')) . "', 'auto');\n";
 $ga .= "ga('send', 'pageview');\n";
}
if (variable_get('uw_cfg_google_analytics_account') && variable_get('google_analytics_enable') == 1) {
  // Removed in favour of Google Tag Manager.
  // @code
  // $ga .= "ga('create', 'UA-51776731-1', 'auto');\n";
  // @endcode
  // New tracker.
  $ga .= "ga('create', '" . check_plain(variable_get('uw_cfg_google_analytics_account')) . "', 'auto', {'name': 'newTracker'});\n";
  $ga .= "ga('send', 'pageview');\n";
  // Send page view for new tracker.
  $ga .= "ga('newTracker.send', 'pageview');\n";
}
$ga .= "</script>";


// Add Google Tag Manager snippets.
if (module_enable('google_tag')) {
  module_load_include('inc', 'google_tag', 'includes/admin');
  module_load_include('inc', 'google_tag', 'includes/snippet');
  $gtm = google_tag_snippets();
}

global $base_path;
if ($_GET['q'] == 'navigation404') {
  include DRUPAL_ROOT . '/apps/uw_theme_conference/404.html';
}
elseif (!path_is_admin(current_path())) {
  include DRUPAL_ROOT . '/apps/uw_theme_conference/index.html';
}
else {
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces; ?>>

<head profile="<?php print $grddl_profile; ?>">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
<div id="skip-link">
  <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
</div>
<?php print $page_top; ?>
<?php print $page; ?>
<?php print $page_bottom; ?>
</body>
</html>

<?php }
